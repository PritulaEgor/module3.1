﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
   static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks  
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source, out int i))
            {
                return Convert.ToInt32(source);
            }
            else
            {
                throw new ArgumentException(source);
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result = Math.Abs(num1);
            for (int i = 2; i <= Math.Abs(num2); i++)
            {
                result += Math.Abs(num1);
            }
            if ((num1 > 0 && num2 > 0) || (num1 < 0 && num2 < 0))
            {
                return result;
            }
            else
            {
                return result * (-1);
            }
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (true)
            {
                if (int.TryParse(input, out int i) && !input.Contains(',') && !input.Contains('.'))
                {
                    result = Convert.ToInt32(input);
                    return true;
                }
                else
                {
                    Console.WriteLine("You entered incorrect value! Please try again");

                    input = Console.ReadLine();

                }
            }

        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            TryParseNaturalNumber(Convert.ToString(naturalNumber), out naturalNumber);
            List<int> result = new List<int>();
            for (int i = 0; ; i++)
            {
                if (result.Count == naturalNumber)
                {
                    break;
                }
                if (i % 2 == 0)
                {
                    result.Add(i);

                }

            }
            return result;

        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {

            while (true)
            {
                if (int.TryParse(input, out int i) && !input.Contains(',') && !input.Contains('.'))
                {
                    result = Convert.ToInt32(input);
                    return true;
                }
                else
                {
                    Console.WriteLine("You entered incorrect value! Please try again");

                    input = Console.ReadLine();

                }
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            TryParseNaturalNumber(Convert.ToString(digitToRemove), out digitToRemove);

            string sourseSt = Convert.ToString(source);

            while (sourseSt.Contains($"{digitToRemove}"))
            {
                sourseSt = sourseSt.Remove(sourseSt.IndexOf(Convert.ToString(digitToRemove)), 1);
                
            }
            return sourseSt;
        }
    }
}
